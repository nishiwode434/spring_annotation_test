package com.wjs.aop;

import com.wjs.annotation.Action;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Created by 18435 on 2017/9/20.
 */

@Aspect
@Component
public class AopTestConfig {

    //切点专门为@after @Before @around 服务
    @Pointcut("@annotation(com.wjs.annotation.Action)")
    private void cut(){
        System.out.println("切点入口。。。。。。。。。。。。。。");
    };


//    @Before("cut()")
//    public void runBeforeMethodTest(JoinPoint joinPoint) {
//        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
//        //反射的方式获取方法名或者注解参数等等
//        Method method = methodSignature.getMethod();
//        String methodName = method.getName();
//        Action action = methodSignature.getMethod().getAnnotation(Action.class);
//        System.out.println("action name is: " + action.name());
//        System.out.println("method name is: " + methodName);
//    }

    /**
     * 这种直接传入action的方式没有调试成功，但是也可以用，有疑惑这里
     */
//    @Before(value = "execution(* com.wjs.service.AnnotationTestService.*(..)) && @annotation(action) ")
//    public void runBeforeMethodTest(Action action , JoinPoint joinPoint) {
//        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
//        //反射的方式获取方法名或者注解参数等等
//        Method method = methodSignature.getMethod();
//        String methodName = method.getName();
//        System.out.println("action name is: " + action.name());
//        System.out.println("method name is: " + methodName);
//    }


    /**
     * execution 测试
     * @param joinPoint
     */
//    @Before(value = "execution(* com.wjs.service.AnnotationTestService.*(..))")
//    public void runBeforeMethodTest( JoinPoint joinPoint) {
//        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
//        //反射的方式获取方法名或者注解参数等等
//        Method method = methodSignature.getMethod();
//        String methodName = method.getName();
//        System.out.println("method name is: " + methodName);
//    }

    /**
     * 联合使用
     * @param joinPoint
     */
    @Before(value = "execution(* com.wjs.service.AnnotationTestService.*(..)) && cut()")
    public void runBeforeMethodTest(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        //反射的方式获取方法名或者注解参数等等
        Method method = methodSignature.getMethod();
        String methodName = method.getName();
        System.out.println("method name is: " + methodName);
    }
}
