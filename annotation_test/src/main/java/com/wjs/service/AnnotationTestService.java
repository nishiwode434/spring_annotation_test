package com.wjs.service;

import com.wjs.annotation.Action;
import org.springframework.stereotype.Service;

/**
 * Created by 18435 on 2017/9/20.
 */

@Service
public class AnnotationTestService {

    @Action(name = "wjs")
    public void testMethod1() {
        System.out.println("testMethod1");
    }

    @Action
    public void testMethod2() {
        System.out.println("testMethod2");
    }
}
