package com.wjs.controler;

import com.wjs.service.AnnotationTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 18435 on 2017/9/20.
 */

@RestController
public class AnnotationController {
    @Autowired
    AnnotationTestService annotationTestService;

    @RequestMapping("/hello")
    public ResponseEntity helloWorldAnnotation() {
        annotationTestService.testMethod1();
        annotationTestService.testMethod2();
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
