package com.wjs.annotation;

import java.lang.annotation.*;

/**
 * Created by 18435 on 2017/9/20.
 */
@Target({ElementType.FIELD, ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Action {
    String name() default "fristAnnotetion";
}